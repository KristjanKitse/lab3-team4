package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Kristjan on 10/10/2016.
 */
public class TeamController implements Initializable {

    private static final Logger log = LogManager.getLogger(TeamController.class);

    InputStream inputStream;

    @FXML
    private Label teamName;
    @FXML
    private Label teamLeader;
    @FXML
    private Label email;
    @FXML
    private Label members;
    @FXML
    private ImageView logo;


    protected void readProps() {
        Properties prop = new Properties();
        String propFileName = "application.properties";

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null) {
            log.info("inputStream: " + inputStream);
        	try {
                log.debug("Properties before inputStream load: " + prop);
            	prop.load(inputStream);
            	log.debug("Properties after inputStream load: " + prop);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }

        String name = prop.getProperty("teamName");
        teamName.setText(name);
        String leader = prop.getProperty("teamLeader");
        teamLeader.setText(leader);
        String email = prop.getProperty("email");
        this.email.setText(email);
        String members = prop.getProperty("members");
        this.members.setText(members);
        String logoURL = prop.getProperty("logo");
        logo.setImage(new Image(logoURL));

    }

    public TeamController() {

    }

    public void initialize(URL location, ResourceBundle resources) {
        readProps();
    }
}
