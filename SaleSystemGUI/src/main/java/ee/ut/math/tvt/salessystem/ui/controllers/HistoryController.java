package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.logic.History;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private static final Logger log = LogManager.getLogger(HistoryController.class);

    private final SalesSystemDAO dao;
    private final History history;
    LocalDate startDate;
    LocalDate endDate;
    HistoryItem selectedItem;

    @FXML
    private Button showBetweenDates;
    @FXML
    private Button showLast10;
    @FXML
    private Button showAll;
    @FXML
    private DatePicker startDateField;
    @FXML
    private DatePicker endDateField;
    @FXML
    private TableView<HistoryItem> historyTableView;
    @FXML
    private TableView<SoldItem> purchaseTableView;


    public HistoryController(SalesSystemDAO dao, History history) {
        this.dao = dao;
        this.history = history;
        startDateField = new DatePicker();
        endDateField = new DatePicker();
        this.selectedItem = new HistoryItem();



    }
	
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        historyTableView.setItems(new ObservableListWrapper<>(history.getAll()));

        purchaseTableView.setItems(new ObservableListWrapper<>(selectedItem.getSoldItems()));

        historyTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<HistoryItem>() {

            @Override
            public void changed(ObservableValue<? extends HistoryItem> observable, HistoryItem oldValue, HistoryItem newValue) {

                selectedItem = newValue;
                purchaseTableView.setItems(new ObservableListWrapper<>(selectedItem.getSoldItems()));
                purchaseTableView.refresh();
            }
        });

    }
    
    @FXML
    protected void showBetweenDatesButtonClicked() {
    	log.info("Show between dates button clicked.");

        try {
        	log.debug("");
            history.showBetweenDates(Date.from(startDateField.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()), Date.from(endDateField.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        historyTableView.refresh();


    }

    @FXML
    protected void showLast10ButtonClicked() {
    	log.info("Show last 10 button clicked.");
        try {
            history.showLast10();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        historyTableView.refresh();

    }

    @FXML
    protected void showAllButtonClicked() {
    	log.info("Show all button clicked.");
        try {
            history.showAll();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        historyTableView.refresh();

    }

    @FXML
    protected void historyItemClicked() {
    	log.info("History item clicked in purchase section.");
        try {
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
