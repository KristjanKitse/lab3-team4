package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StockController implements Initializable {

    private static final Logger log = LogManager.getLogger(StockController.class);

	//Test 05.10.2016
	//Test
    private final SalesSystemDAO dao;
    private final Warehouse warehouse;

    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao, Warehouse warehouse) {

        this.dao = dao;
        this.warehouse = warehouse;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
        // TODO refresh view after adding new items
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @FXML
    public void addItemButtonClicked() {
        // add chosen item to the shopping cart.
        //StockItem stockItem = getStockItemByBarcode();
        StockItem stockItem = getStockItemByBarcode();
        //if(stockItem==null)
            //stockItem = new StockItem(Long.parseLong(barCodeField.getText()),nameField.getText(),"",Double.parseDouble(priceField.getText()),Integer.parseInt(quantityField.getText()));
        warehouse.addItem(stockItem,barCodeField.getText(),nameField.getText(),"",priceField.getText(),quantityField.getText());
        refreshStockItems();

    }

    @FXML
    public void refreshButtonClicked() {
        log.info("Refresh warehouse button clicked.");
        try {

            log.debug("Stock Items before refresh:\n"); //add list of stock items before click
            refreshStockItems();
            log.debug("Stock Items after refresh:\n"); //add list of stock items after click
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(new ObservableListWrapper<>(dao.findStockItems()));
        warehouseTableView.refresh();
    }

}
