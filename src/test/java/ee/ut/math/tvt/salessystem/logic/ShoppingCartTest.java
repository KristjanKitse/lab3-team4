package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertTrue;

public class ShoppingCartTest {

	private SalesSystemDAO dao;
	private ShoppingCart shoppingCart;
	private StockItem stockItem1;
    private StockItem stockItem2;


	@Before
	public void setUp() {
		dao = new InMemorySalesSystemDAO();
		shoppingCart = new ShoppingCart(dao);
		stockItem1 = dao.findStockItems().get(0);
        stockItem2 = dao.findStockItems().get(1);

	}


//	Tests for addItem
	@Test
	public void testAddingExistingItem() {
//		check that adding an existing item increases the quantity


		shoppingCart.addItem(new SoldItem(stockItem1,1));
		shoppingCart.addItem(new SoldItem(stockItem1,1));
		assertTrue(shoppingCart.getAll().get(0).getQuantity()==2);

		shoppingCart.cancelCurrentPurchase();
		
	}

	@Test
	public void testAddingNewItem() {
//		check that the new item is added to the shopping cart
        SoldItem soldItem = new SoldItem(stockItem2,2);
		shoppingCart.addItem(soldItem);

		assertTrue(shoppingCart.getAll().get(shoppingCart.getAll().size()-1).equals(soldItem));

		shoppingCart.cancelCurrentPurchase();
		
	}

	@Test(expected=SalesSystemException.class)
	public void testAddingItemWithNegativeQuantity() {
//		check that an exception is thrown if trying to add item with negative quantity
        SoldItem item = new SoldItem(stockItem1, -1);
        shoppingCart.addItem(item);
	}



	@Test(expected=SalesSystemException.class)
	public void testAddingItemWithQuantityTooLarge() {
//		check that an exception is thrown if the quantity of 
//		the added item is larger than quantity in warehouse
        SoldItem item = new SoldItem(stockItem1, 999999999);
        shoppingCart.addItem(item);

	}

	@Test
	public void testAddingItemWithQuantitySumTooLarge() {
//		check that an exception is thrown if the sum of the quantity of 
//		the added item and the quantity already in shopping cart 
//		is larger than quantity in warehouse

        boolean thrown = false;
        shoppingCart.addItem(new SoldItem(stockItem1,4));
        try{
            shoppingCart.addItem(new SoldItem(stockItem1,3));
        }
        catch (SalesSystemException e){
            thrown = true;
        }
        assertTrue(thrown);

	}

//	Tests for submitCurrentPurchase
	@Test
	public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
//		check that submitting the current purchase 
//		decreases the quantity of all StockItems
        int q1 = dao.findStockItem(stockItem1.getId()).getQuantity();
        int q2 = dao.findStockItem(stockItem2.getId()).getQuantity();

        shoppingCart.addItem(new SoldItem(stockItem1,2));
		shoppingCart.addItem(new SoldItem(stockItem2,2));

        shoppingCart.submitCurrentPurchase();

        boolean result = false;

        if(dao.findStockItem(stockItem1.getId()).getQuantity()==(q1-2) &&
                dao.findStockItem(stockItem2.getId()).getQuantity()==(q2-2)){
            result = true;
        }

        assertTrue(result);
	}

	@Test
	public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
//		check that submitting the current purchase calls beginTransaction 
//		and endTransaction, exactly once and in that order
        boolean result = false;

        shoppingCart.submitCurrentPurchase();

        if (dao.getBeginTransactionCounter() == 1 || dao.getCommitTransactionCounter() == 1) {
            result = true;
        }
        if (dao.getBeginTransactionBeforeCommitTransaction() == true) {
            result = true;
        }
        assertTrue(result);
		
	}

	@Test
	public void testSubmittingCurrentOrderCreatesHistoryItem() {
//		check that a new HistoryItem is saved and that it contains the correct SoldItems
        int initialSize = dao.findHistoryItem().size();

        SoldItem s1 = new SoldItem(stockItem1,2);
        SoldItem s2 = new SoldItem(stockItem2,2);

        shoppingCart.addItem(s1);
        shoppingCart.addItem(s2);

        shoppingCart.submitCurrentPurchase();

        boolean result = false;

        if(dao.findHistoryItem().size()==(initialSize+1) &&
                dao.findHistoryItem().get(0).getSoldItems().contains(s1) &&
                dao.findHistoryItem().get(0).getSoldItems().contains(s2)){

            result = true;
        }

        assertTrue(result);
	}

	@Test
	public void testSubmittingCurrentOrderSavesCorrectTime() {
//		check that the timestamp on the created HistoryItem is set correctly 
//		(for example has only a small difference to the current time)
        boolean result = false;
        Date currentDate = new Date();
        shoppingCart.submitCurrentPurchase();
        Date itemDate = dao.findHistoryItem().get(0).getDate();

        if(itemDate.equals(currentDate))
            result = true;

        assertTrue(result);

    }

	@Test
	public void testCancellingOrder() {
//		check that cancelling an order (with some items) and then submitting 
//		a new order (with some different items) only saves the items from 
//		the new order (cancelled items are discarded)
        SoldItem soldItem1 = new SoldItem(stockItem1,2);
        SoldItem soldItem2 = new SoldItem(stockItem2,2);

        shoppingCart.addItem(soldItem1);
        shoppingCart.cancelCurrentPurchase();
        shoppingCart.addItem(soldItem2);
        shoppingCart.submitCurrentPurchase();


        boolean result = true;

        for(SoldItem i : dao.findSoldItems()){
            if(i.equals(soldItem1)) {
                result = false;
            }
            if(!i.equals(soldItem2)){
                result = false;
            }
        }

        assertTrue(result);
		
	}

	@Test
	public void testCancellingOrderQuanititesUnchanged() {
//		check that after cancelling an order the quantities of 
//		the related StockItems are not changed
        int q1 = dao.findStockItem(stockItem1.getId()).getQuantity();
        int q2 = dao.findStockItem(stockItem2.getId()).getQuantity();

        shoppingCart.addItem(new SoldItem(stockItem1,2));
        shoppingCart.addItem(new SoldItem(stockItem2,2));

        shoppingCart.cancelCurrentPurchase();

        boolean result = true;

        if(dao.findStockItem(stockItem1.getId()).getQuantity()==(q1-2) ||
                dao.findStockItem(stockItem2.getId()).getQuantity()==(q2-2)){
            result = false;
        }

        assertTrue(result);
		
	}

}
