package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class WarehouseTest {

	private SalesSystemDAO dao;
	private Warehouse warehouse;
	private StockItem stockItem1;

    @Before
    public void setUp() {
		dao = new InMemorySalesSystemDAO();
		warehouse = new Warehouse(dao);
		stockItem1 = dao.findStockItems().get(0);
        
    }
    
	@Test
	public void testAddingItemBeginsAndCommitsTransaction() {
//		check that methods beginTransaction and commitTransaction
//		are both called exactly once and that order
		boolean result = false;
		
		warehouse.addItem(stockItem1, 
				stockItem1.getId().toString(), 
				stockItem1.getName(), 
				stockItem1.getDescription(), 
				Double.toString(stockItem1.getPrice()), 
				"1");
		
		if (dao.getBeginTransactionCounter() == 1 || dao.getCommitTransactionCounter() == 1) {
			result = true;
		}
		if (dao.getBeginTransactionBeforeCommitTransaction() == true) {
			result = true;
		}
		assertTrue(result);
	}

	@Test
	public void testAddingNewItem() {
//		check that a new item is saved through the DAO
		boolean result = true;
		int stockItemCountBefore = dao.findStockItems().size();
		
		warehouse.addItem(null,
				"5L",
				"New item",
				"Test",
				"10.0",
				"1");

		int stockItemCountAfter = dao.findStockItems().size();

		if (stockItemCountBefore>=stockItemCountAfter){

			result = false;
		}
		assertTrue(result);
	}

	@Test
	public void testAddingExistingItem() {
//		check that adding a new item increases the quantity 
//		and the saveStockItem method of the DAO is not called
		boolean result = true;
		int q1 = dao.findStockItem(stockItem1.getId()).getQuantity();
		
		warehouse.addItem(stockItem1, 
				stockItem1.getId().toString(), 
				stockItem1.getName(), 
				stockItem1.getDescription(), 
				Double.toString(stockItem1.getPrice()), 
				"1");

		int q2 = dao.findStockItem(stockItem1.getId()).getQuantity();
		
		if (q1 == q2){
			result = false;
		}
		if (dao.getSaveStockItemCounter() > 0) {
			result = false;
		}
		assertTrue(result);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testAddingItemWithNegativeQuantity() {
//		check that adding an item with negative quantity results in an exception
		warehouse.addItem(stockItem1, 
				stockItem1.getId().toString(), 
				stockItem1.getName(), 
				stockItem1.getDescription(), 
				Double.toString(stockItem1.getPrice()), 
				"-1");
	}
}
