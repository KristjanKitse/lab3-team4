package ee.ut.math.tvt.salessystem;

import javax.swing.*;

/**
 * Base class for sales system exceptions
 */
public class SalesSystemException extends RuntimeException {

    public SalesSystemException() {
        super();
    }

    public SalesSystemException(String message) {
        super(message);

        JOptionPane.showMessageDialog(null, message, "ERROR", JOptionPane.ERROR_MESSAGE);
    }

    public SalesSystemException(String message, Throwable cause) {
        super(message, cause);
    }
}
