package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "HistoryItem")
public class HistoryItem {


	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;

	@Column(name="date")
    private Date date;

	@Column(name="total")
    private double total;

	@OneToMany(mappedBy = "historyItem")
	private List<SoldItem> soldItems;

	public HistoryItem(){}

	public HistoryItem(Date date, double total, List<SoldItem> soldItems){
		this.date = date;
		this.total = total;
		this.soldItems = soldItems;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) { this.date = date; }

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public List<SoldItem> getSoldItems(){
		if(soldItems != null) {
			List<SoldItem> items = new ArrayList<>();
			items = soldItems;
			return items;
		}
		return new ArrayList<SoldItem>();

	}

	public void setSoldItems(List<SoldItem> soldItems){this.soldItems = soldItems;}

}