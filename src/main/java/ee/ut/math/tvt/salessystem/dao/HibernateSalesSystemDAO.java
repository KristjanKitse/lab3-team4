package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

	private final EntityManagerFactory emf;
	private final EntityManager em;

	public HibernateSalesSystemDAO() {
		// if you get ConnectException/JDBCConnectionException then you
		// probably forgot to start the database before starting the application
		emf = Persistence.createEntityManagerFactory("pos");
		em = emf.createEntityManager();
	}


	public void close() {
		em.close();
		emf.close();
	}

	@Override
	public void beginTransaction() {
		em.getTransaction().begin();
	}

	@Override
	public void rollbackTransaction() {
		em.getTransaction().rollback();
	}

	@Override
	public void commitTransaction() {
		em.getTransaction().commit();
	}

	@Override
	public int getBeginTransactionCounter() {
		return 0;
	}

	@Override
	public int getCommitTransactionCounter() {
		return 0;
	}

	@Override
	public boolean getBeginTransactionBeforeCommitTransaction() {
		return false;
	}

	@Override
	public int getSaveStockItemCounter() {
		return 0;
	}

	@Override
	public List<StockItem> findStockItems() {
		return em.createQuery("from StockItem",StockItem.class).getResultList();

	}

	@Override
	public List<SoldItem> findSoldItems() {
		return em.createQuery("from SoldItem",SoldItem.class).getResultList();
	}

	@Override
	public StockItem findStockItem(long id) {
		List<StockItem> stockItems = em.createQuery("from StockItem",StockItem.class).getResultList();
		for(StockItem item : stockItems){
			if(item.getId()==id)
				return item;
		}
		return null;
	}

	@Override
	public StockItem findStockItemByName(String name) {
		List<StockItem> stockItems = em.createQuery("from StockItem",StockItem.class).getResultList();
		for(StockItem item : stockItems){
			if(item.getName().equals(name))
				return item;
		}
		return null;
	}

	@Override
	public void saveStockItem(StockItem stockItem) {
	    //em.setProperty("StockItem",stockItem);
	    em.persist(stockItem);
		/*List<StockItem> stockItems = em.createQuery("from StockItem",StockItem.class).getResultList();
		stockItems.add(stockItem);*/

	}

	@Override
	public void saveSoldItem(SoldItem item) {
		em.persist(item);

	}

	@Override
	public void saveHistoryItem(HistoryItem historyItem) {
		em.persist(historyItem);
	}

	@Override
	public List<HistoryItem> findHistoryItem() {
		List<HistoryItem> historyItemList = em.createQuery("from HistoryItem",HistoryItem.class).getResultList();
		return historyItemList;
	}
}