package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;

public class History {

	private static final Logger log = LogManager.getLogger(History.class);
	private final SalesSystemDAO dao;
	private final ArrayList<HistoryItem> purchaseHistoryItems = new ArrayList<>();

	public History(SalesSystemDAO dao) {
		this.dao = dao;
	}


	public void showBetweenDates(Date startdate, Date enddate){

		purchaseHistoryItems.clear();
		
		if (startdate.after(enddate)){
			throw new SalesSystemException("End date " + enddate + " is earlier than Start date " + startdate + ".");
		}
		if (startdate.after(new Date())){
			throw new SalesSystemException("Start date " + startdate + " is later than the current date " + new Date() + ".");
		}

		try {
			for (HistoryItem item : dao.findHistoryItem()){
				if(item.getDate().after(startdate) && item.getDate().before(enddate)){
					purchaseHistoryItems.add(item);
				}
			}
		}
		catch (Exception e){
			throw new SalesSystemException("History is empty.");
		}

	}

	public void showAll(){
		purchaseHistoryItems.clear();
		if (dao.findHistoryItem().size() > 0) {
			try {

				for(HistoryItem item: dao.findHistoryItem()){
					purchaseHistoryItems.add(item);
				}
			}

			catch (Exception e) {
				throw new SalesSystemException("'Show all' error.");
			}
		}
		else {
			throw new SalesSystemException("History is empty.");
		}
	}

	
	public void showLast10(){

		purchaseHistoryItems.clear();

		try {
			if (dao.findHistoryItem().size() < 10) {
				int j = dao.findHistoryItem().size();
				for (int i = 1; i <= j; i++){
					purchaseHistoryItems.add(dao.findHistoryItem().get(j-i));
				}
				if (j == 0) {
					throw new SalesSystemException("History is empty.");
				}
				else {
					throw new SalesSystemException("Less than 10 sales in history. Returned " + j + " result(s).");
				}
			}
			else{
				for (int i = 1; i < 11; i++){
					purchaseHistoryItems.add(dao.findHistoryItem().get(dao.findHistoryItem().size()-i));
				}
			}
		}
		catch (Exception e) {
			throw new SalesSystemException("Purchase history population error.");
		}
	}
	
	public ArrayList<HistoryItem> getAll(){
		return purchaseHistoryItems;
	}


}
