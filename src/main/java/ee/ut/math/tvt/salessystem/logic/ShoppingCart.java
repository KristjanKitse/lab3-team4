package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {

        if (item.getQuantity()<0){
            throw new SalesSystemException("Amount must be positive");
        }


        for (SoldItem i : items){
            long id = item.getStockItem().getId();

            if (i.getStockItem().getId() == id) {
                if(dao.findStockItem(id).getQuantity() >= item.getQuantity() + i.getQuantity()) {
                    i.setQuantity(i.getQuantity() + item.getQuantity());
                }
                else{
                    throw new SalesSystemException("max quantity exceeded");
                }
                return;
            }

        }

        if(dao.findStockItem(item.getStockItem().getId()).getQuantity() >= item.getQuantity()){
            items.add(item);

        }
        else{
            throw new SalesSystemException("max quantity exceeded");


        }


        //log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();

        Date date = new Date();

        try {
            double total = 0;
            List<SoldItem> soldItems = new ArrayList<>();
            for (SoldItem item : items) {
                total += item.getSum();
                int quantity = dao.findStockItem(item.getStockItem().getId()).getQuantity();
                dao.findStockItem(item.getStockItem().getId()).setQuantity(quantity-item.getQuantity());

                soldItems.add(item);

            }

            HistoryItem historyItem = new HistoryItem(date, total, soldItems);

            for(SoldItem item: items){
                item.setHistoryItem(historyItem);

                dao.saveSoldItem(item);

            }

            dao.saveHistoryItem(historyItem);

            dao.commitTransaction();
            items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

}
