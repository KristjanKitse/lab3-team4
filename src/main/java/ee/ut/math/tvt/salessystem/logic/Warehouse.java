package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karl-jaak on 01/11/2016.
 */
public class Warehouse {
    private final SalesSystemDAO dao;

    private List<StockItem> items = new ArrayList<>();


    public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public void addItem(StockItem item,String ID,String itemName,String itemDesc,String price,String quantity){


        long itemID = 0;
        double itemPrice = 0;
        int itemQuantity = 0;
        boolean isInTheWareHouseAlready = false;
        if(itemName.equals("")){
            throw new SalesSystemException("Wrong data");
        }
        if (Integer.parseInt(quantity) < 0) {
        	throw new IllegalArgumentException("Quantity cannot be negative.");
        }
        try {
            itemID = Long.parseLong(ID);
            itemPrice = Double.parseDouble(price);
            itemQuantity = Integer.parseInt(quantity);
        }
        catch(NumberFormatException e){
            try {

                itemPrice = Double.parseDouble(price);
                itemQuantity = Integer.parseInt(quantity);
                dao.beginTransaction();
                items = dao.findStockItems();
                dao.commitTransaction();
                for(StockItem i : items){
                    if(itemName.equals(i.getName())){
                        itemID=i.getId();
                        isInTheWareHouseAlready=true;
                    }
                }
                if(!isInTheWareHouseAlready) {
                    try {
                        itemID = items.get(items.size() - 1).getId() + 1;
                    }
                    catch(Exception f){
                        itemID=1;
                    }

                }


            }
            catch(NumberFormatException ex) {
                dao.rollbackTransaction();
                throw new SalesSystemException("Wrong data");
            }

        }
        if(isInTheWareHouseAlready){
            for(StockItem i : items){
                if(itemName.equals(i.getName())){
                    item=i;
                }
            }
        }


        if(item==null) {



            item = new StockItem(itemID, itemName, itemDesc, itemPrice, itemQuantity);


            dao.beginTransaction();
            dao.saveStockItem(item);
            dao.commitTransaction();



        }
        else{



            if(item.getPrice()!=itemPrice){
                throw new SalesSystemException("Price and ID do not match");
            }
            if(item.getName().toUpperCase().equals(itemName.toUpperCase())==false){
                throw new SalesSystemException("Name and ID do not match");
            }
            int previousQuantity = item.getQuantity();
            item.setQuantity(previousQuantity+itemQuantity);

        }

    }
}
