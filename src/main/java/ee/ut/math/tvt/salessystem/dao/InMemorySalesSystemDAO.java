package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<HistoryItem> historyItemList;

    private int saveStockItemCounter = 0;
	private int beginTransactionCounter = 0;
    private int commitTransactionCounter = 0;
    private boolean beginTransactionBeforeCommitTransaction = true;

    public int getSaveStockItemCounter() {
		return saveStockItemCounter;
	}

	public int getBeginTransactionCounter() {
		return beginTransactionCounter;
	}

	public int getCommitTransactionCounter() {
		return commitTransactionCounter;
	}

	public boolean getBeginTransactionBeforeCommitTransaction() {
		return beginTransactionBeforeCommitTransaction;
	}

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sausages", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.historyItemList = new ArrayList<>();
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public List<SoldItem> findSoldItems() { return soldItemList; }

    @Override
    public List<HistoryItem> findHistoryItem() { return historyItemList; }


    @Override
    public StockItem findStockItemByName(String name) {
        for (StockItem item : stockItemList) {
            if (item.getName().equals(name))
                return item;
        }
        return null;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveHistoryItem(HistoryItem historyItem) {
        historyItemList.add(historyItem);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
    	saveStockItemCounter++;
        stockItemList.add(stockItem);
    }

    @Override
    public void beginTransaction() {
    	if (commitTransactionCounter > beginTransactionCounter){
    		beginTransactionBeforeCommitTransaction = false;
    	}
    	beginTransactionCounter++;
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    	if (commitTransactionCounter == beginTransactionCounter ||
    			commitTransactionCounter > beginTransactionCounter){
    		beginTransactionBeforeCommitTransaction = false;
    	}
    	commitTransactionCounter++;
    }
}
